## Maxime Rossier CodeTest

I started to try to upload a file with the android drive API, and I saw that I couldn't interact with the files that I didn't opened or created with this app. 
So I used the Java drive REST API to list and download the files. The files are downloaded on the cache. To open any file, I use a content provider. 
To open a PDF I should probably make a custom FileProvider but I checked the extention to be quick. If I open a PDF, I use my own Activity to open it with PdfRenderer. To see the share button you have to touch the screen.

## improvement
- improve the design
- add the thumbnails on the gallery
- create a menu with a the log off option
- remove the "get drive" once we're logged in and add a swipeToRefresh view to refresh the list
- make a FloatingActionButton to share the file and add a share option on the MainActivity
- get more files in the gallery if I scroll down or get all the files directly
- create some classes to put the general methods like sharing/opening a file render a PDF or any method that I could use in differents places
- add the UploadActivity to the app
- change some names and use the string.xml attributes for every string that I hardcoded
- add some option to edit the files or permanently download them