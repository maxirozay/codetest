package com.max.codetest;

import com.google.api.services.drive.model.File;

/**
 * Created by Maxime Rossier on 8/26/2016.
 */
public interface OnListInteractionListener {
    void onClick(File file);
    void onShare(File file);
}
